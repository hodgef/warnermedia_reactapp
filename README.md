# Warner Media React App POC

## Features

- Router
- Unit Tests
- Component-based structure
- Deployment scripts
- Code minification for better performance
- Using ES6+ Javascript Standard Class syntax
- Using native Javascript imports, no need for require.js
- Hot reloading for quick prototyping
- And more!

## Stack

- Webpack 4
- Babel 7
- React 16