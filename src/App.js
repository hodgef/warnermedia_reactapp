import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { withRouter, Redirect } from 'react-router';
import PropTypes from 'prop-types';

import './App.css';

import Home from './screens/Home';
import MainLobbyWrap from './screens/MainLobbyWrap';

class App extends Component {
  
  render() {
    return (
      <div className={"App"}>
        <Switch>
          <Route path="/home" render={(props) => 
            <Home
              {...props}
            />}
          />

          <Route path="/mainLobbyWrap" render={(props) => 
            <MainLobbyWrap
              {...props}
            />}
          />

          <Route render={() => 
            <Redirect to={"/home"}/>
          } />
        </Switch>
      </div>
    );
  }
}

export default withRouter(App);