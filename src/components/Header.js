import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './css/Header.css';

class App extends Component {
  
  render() {
    return (
      <div className={"Header"}>
        I'm a header
      </div>
    );
  }
}

export default App;